module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: "airbnb-base",
  overrides: [
  ],
  parser: "@babel/eslint-parser",
  parserOptions: {
    ecmaVersion: "latest",
  },
  rules: {
    "class-methods-use-this": ["error", { "exceptMethods": ["createProgram", "remove"] }],
    "no-use-before-define": ["error", { "functions": false }],
    "lines-between-class-members": ["error", "never"],
    "no-console": "off",
    "semi": "off",
    "prefer-const": "off",
    "quotes": "off",
    "padded-blocks": "off",
    "consistent-return": "off",
    "default-case": "off",
    "no-alert": "off"
  },
};
