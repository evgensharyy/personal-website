/* eslint max-len: ["error", { "ignoreStrings": true }] */

class SectionCreator {
  createProgram(type) {
    switch (type) {
      case 'standart':
        return standartProgram()
      case 'advanced':
        return advancedProgram()
    }
  }
  remove() {
    return removeProgram()
  }
}

function standartProgram() {

  let footer = document.querySelector(".app-footer");

  // App Section

  let appJoin = document.createElement('section');
  appJoin.classList.add('app-join', 'app-join--image-peak');

  // App Section

  // App Title

  let appTitle = document.createElement('h2');
  appTitle.classList.add('app-title');
  appTitle.innerHTML = 'Join Our Program';

  // App Title

  // App SubTitle
  let appSubTitle = document.createElement('h3');
  appSubTitle.classList.add('app-subtitle');
  appSubTitle.innerHTML = 'Sed do eiusmod tempor incididunt<br /> ut labore et dolore magna aliqua.';

  // App SubTitle

  // App Join Form

  let appJoinForm = document.createElement('form');
  appJoinForm.classList.add('app-join__form');

  // Join Form Input Email

  let joinFormInputEmail = document.createElement('input');
  joinFormInputEmail.type = 'text';
  joinFormInputEmail.name = 'email';
  joinFormInputEmail.value = 'Email';

  // Join Form Input Email

  let joinFormInputButton = document.createElement('input');
  joinFormInputButton.classList.add('app-join__button', 'app-join__button--subscribe');
  joinFormInputButton.type = 'submit';
  joinFormInputButton.value = 'SUBSCRIBE';

  // App Join Form

  // ************ LOAD ****************

  footer.parentNode.insertBefore(appJoin, footer);

  appJoin.append(appTitle, appSubTitle, appJoinForm);

  appJoinForm.append(joinFormInputEmail, joinFormInputButton);

  let appJoinFormElement = document.querySelector(".app-join__form");

  appJoinFormElement.addEventListener('submit', (event) => {
    event.preventDefault();
    console.log(joinFormInputEmail.value);
  });

}

function advancedProgram() {

  let footer = document.querySelector(".app-footer");

  // App Section

  let appJoin = document.createElement('section');
  appJoin.classList.add('app-join', 'app-join--image-peak');

  // App Section

  // App Title

  let appTitle = document.createElement('h2');
  appTitle.classList.add('app-title');
  appTitle.innerHTML = 'Join Our Advanced Program';

  // App Title

  // App SubTitle
  let appSubTitle = document.createElement('h3');
  appSubTitle.classList.add('app-subtitle');
  appSubTitle.innerHTML = 'Sed do eiusmod tempor incididunt<br /> ut labore et dolore magna aliqua.';

  // App SubTitle

  // App Join Form

  let appJoinForm = document.createElement('form');
  appJoinForm.classList.add('app-join__form');

  // Join Form Input Email

  let joinFormInputEmail = document.createElement('input');
  joinFormInputEmail.type = 'text';
  joinFormInputEmail.name = 'email';
  joinFormInputEmail.value = 'Email';

  // Join Form Input Email

  let joinFormInputButton = document.createElement('input');
  joinFormInputButton.classList.add('app-join__button', 'app-join__button--subscribe');
  joinFormInputButton.type = 'submit';
  joinFormInputButton.value = 'Subscribe to Advanced Program';
  joinFormInputButton.style.width = "300px";

  // App Join Form

  // ************ LOAD ****************

  footer.parentNode.insertBefore(appJoin, footer);

  appJoin.append(appTitle, appSubTitle, appJoinForm);

  appJoinForm.append(joinFormInputEmail, joinFormInputButton);

  let appJoinFormElement = document.querySelector(".app-join__form");

  appJoinFormElement.addEventListener('submit', (event) => {
    event.preventDefault();
    console.log(joinFormInputEmail.value);
  });

}

function removeProgram() {
  document.querySelector(".app-join").remove();
}

export default SectionCreator;
