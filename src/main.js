import validateEmail from './email-validator'
import './styles/style.css'
import './styles/normalize.css'

function standartProgram() {

  let footer = document.querySelector(".app-footer");

  // App Section

  let appJoin = document.createElement('section');
  appJoin.classList.add('app-join', 'app-join--image-peak');

  // App Title

  let appTitle = document.createElement('h2');
  appTitle.classList.add('app-title');
  appTitle.innerHTML = 'Join Our Program';

  // App SubTitle

  let appSubTitle = document.createElement('h3');
  appSubTitle.classList.add('app-subtitle');
  appSubTitle.innerHTML = 'Sed do eiusmod tempor incididunt<br /> ut labore et dolore magna aliqua.';

  // App Join Form

  let appJoinForm = document.createElement('form');
  appJoinForm.classList.add('app-join__form');

  // Join Form Input Email

  let joinFormInputEmail = document.createElement('input');
  joinFormInputEmail.classList.add('app-join__input');
  joinFormInputEmail.type = 'text';
  joinFormInputEmail.name = 'email';
  joinFormInputEmail.value = '';

  // Join Form Input Email

  let joinFormInputButton = document.createElement('input');
  joinFormInputButton.classList.add('app-join__button', 'app-join__button--subscribe');
  joinFormInputButton.type = 'submit';
  joinFormInputButton.value = 'SUBSCRIBE';

  // ************ LOAD ****************

  footer.parentNode.insertBefore(appJoin, footer);
  appJoin.append(appTitle, appSubTitle, appJoinForm);
  appJoinForm.append(joinFormInputEmail, joinFormInputButton);
  let appJoinFormElement = document.querySelector(".app-join__form");

  appJoinFormElement.addEventListener('submit', (event) => {
    event.preventDefault();

    if (validateEmail(joinFormInputEmail.value)) {

      let users = JSON.parse(localStorage.getItem('users'));

      users.push(localStorage.getItem('email').toString());

      localStorage.setItem('users', JSON.stringify(users));

      document.location.reload();

    }
    if (!validateEmail(joinFormInputEmail.value)) {
      alert(`Email: ${joinFormInputEmail.value} is not valid! Pleas try again`)
    }
  });

  joinFormInputEmail.addEventListener('input', function (e) {
    localStorage.setItem('email', e.target.value);
  }, false);

}

window.addEventListener("load", standartProgram());



if (localStorage.getItem('users') === null || JSON.parse(localStorage.getItem('users')).length === 0) {

  if (localStorage.getItem('users') === null) {
    let emptyUsers = [];
    localStorage.setItem('users', JSON.stringify(emptyUsers));
  }

} else if (JSON.parse(localStorage.getItem('users')).includes(localStorage.getItem('email'))) {

  let joinFormInputEmail = document.querySelector(".app-join__input");
  joinFormInputEmail.style.display = 'none';

  let joinFormInputButton = document.querySelector(".app-join__button");
  joinFormInputButton.style.display = 'none';

  let subscriberFormInputButton = document.createElement('input');
  subscriberFormInputButton.classList.add('app-join__button', 'app-join__button--subscribe', 'app-join__button--unsubscribe');
  subscriberFormInputButton.type = 'submit';
  subscriberFormInputButton.value = 'UNSUBSCRIBE';

  let appJoin = document.querySelector(".app-join");
  appJoin.append(subscriberFormInputButton);

  subscriberFormInputButton.addEventListener('click', (event) => {
    event.preventDefault();

    let users = JSON.parse(localStorage.getItem('users'));

    let index = users.indexOf(localStorage.getItem('email').toString());

    if (index !== -1) {
      users.splice(index, 1);
    }

    localStorage.setItem('users', JSON.stringify(users));
    localStorage.setItem('email', '');

    document.location.reload();
  });

}
