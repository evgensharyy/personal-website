const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export default function validate(email) {
  const getDomain = email.substring(email.indexOf('@') + 1);

  return VALID_EMAIL_ENDINGS.includes(getDomain);
}
